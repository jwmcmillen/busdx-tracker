# busdx tracker

A very (and I do mean very) simple reflection tracker for busdx busd rewards. Requires you to get your own bscscan api token (they are free).

## Usage
1. Download the project
2. Open the busdx-tracker/js/main.js file
3. Follow the header (comments) instructions for getting a bscscan account and an API Key and setting it in this file
4. Save changes
5. Open index.html
6. Enter public wallet address
7. Click 'Get Reflections'
8. Let me know how it goes

![](img/screenshot.png)

## License
MIT License - Do as you will
