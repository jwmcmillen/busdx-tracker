/**
 * How-to get this JS to function
 * 1. Go To: https://docs.bscscan.com/
 * 2. Follow 'Creating an Account'
 * 3. Follow 'Getting an API Key'
 * 4. Copy newly generated key into API_KEY value (over INSERT_API_KEY_HERE text)
 */
var API_KEY = 'INSERT_API_KEY_HERE';

var API_CONTEXT='https://api.bscscan.com/api';
var API_MODULE='account';
var API_ACTION='tokentx';
var BUSD_CONTRACT='0xe9e7cea3dedca5984780bafc599bd69add087d56';

var API_BLOCK_START=0;
var API_BLOCK_END=999999999;
var API_SORT='desc';

var FILTER_FROM='0x40d8e3f9a61c965ec5cc8b9cb06d39899e60311d';

function getReflectionData(){
  var walletAddress = document.getElementById('wallet-address').value;
  var request = new XMLHttpRequest();
  var totalDiv = 0;
  var URL_PREPEND=API_CONTEXT+'?module='+API_MODULE+'&action='+API_ACTION+'&contractaddress='+BUSD_CONTRACT+'&address=';
  var URL_APPEND='&startblock='+API_BLOCK_START+'&endblock='+API_BLOCK_END+'&sort='+API_SORT+'&apikey='+API_KEY;
  request.responseType = 'json';
  request.onreadystatechange = function(){
    if( request.readyState == 4 && request.status == 200 ){
      var response = request.response.result;
      document.getElementById( 'reflection-list' ).innerHTML = "";
      for( var result in response ){
        if( response[ result ].from == FILTER_FROM ){
          var amount = parseInt( response[ result ].value );
          var value =  amount / 1000000000000000000;
          totalDiv += value;
          var timestamp = new Date( parseInt( response[ result ].timeStamp ) * 1000 );
          printReflectionList( value , timestamp.toLocaleString() );
        }
      }
      document.getElementById('total-reflections').textContent = '$'+totalDiv;
    }
  }  
  request.open( 'GET', URL_PREPEND+walletAddress+URL_APPEND , true );
  request.send();
}

function printReflectionList( value, date ){
  var reflectionList = document.getElementById( 'reflection-list' );
  var reflection = `<div class='reflection'><span class='date'>${date}</span><span class='filler'></span><span class='value'>${value}</span></div>`;
  reflectionList.insertAdjacentHTML( 'beforeend', reflection );
}

